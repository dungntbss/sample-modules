<?php
declare(strict_types=1);
namespace Bss\FormSample\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ArticleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get articles list.
     *
     * @return ArticleInterface[]
     */
    public function getItems();

    /**
     * Set articles list.
     *
     * @param ArticleInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
