<?php
declare(strict_types=1);
namespace Bss\FormSample\Model\ResourceModel\Article;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Bss\FormSample\Model\Article;
use Bss\FormSample\Model\ResourceModel\Article as ResourceArticle;

class Collection extends AbstractCollection
{
    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(Article::class, ResourceArticle::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }

    /**
     * @return $this
     */
    public function delete()
    {
        $ids = $this->getAllIds();
        if (!empty($ids)) {
            $this->getConnection()->delete($this->getMainTable(), [$this->getIdFieldName() . ' IN (?)' => $ids]);
        }
        return $this;
    }
}
