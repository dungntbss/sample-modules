<?php
declare(strict_types=1);
namespace Bss\FormSample\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Topic implements OptionSourceInterface
{
    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        return [
            ['value' => __('Sport'), 'label' => __('Sport')],
            ['value' => __('Tips'), 'label' => __('Tips and tricks')]
        ];
    }

    /**
     * Convert OptionArray to array
     *
     * @return array
     */
    public function toArray()
    {
        $options = [];
        $optionArray = $this->toOptionArray();
        foreach ($optionArray as $opt) {
            $options[$opt['value']] = $opt['label'];
        }
        return $options;
    }
}
