<?php
declare(strict_types=1);
namespace Bss\FormSample\Model\Article;

use Bss\FormSample\Model\Article;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Mime;
use Magento\Framework\File\Uploader;
use Magento\Framework\Filesystem;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class PreviewImage
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var Mime
     */
    protected $mime;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var string
     */
    protected $fieldName = Article::PREVIEW_IMG;

    /**
     * @var string
     */
    protected $prefix = 'tmp_';

    /**
     * PreviewImage constructor.
     * @param Filesystem $filesystem
     * @param ImageUploader $imageUploader
     * @param Mime $mime
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        LoggerInterface $logger,
        Filesystem $filesystem,
        ImageUploader $imageUploader,
        Mime $mime,
        StoreManagerInterface $storeManager
    ) {
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->imageUploader = $imageUploader;
        $this->mime = $mime;
        $this->storeManager = $storeManager;
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @return Store
     */
    protected function getStore()
    {
        if (!$this->store) {
            $this->store = $this->storeManager->getStore();
        }
        return $this->store;
    }

    /**
     * @param $image
     * @return array
     */
    public function getImageData($image)
    {
        try {
            $basePathUrl = $this->imageUploader->getBasePath() . "/" . $image;
            $basePathDirectory = DIRECTORY_SEPARATOR . $basePathUrl;
            $mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
            $statImage = $mediaDirectory->stat($basePathDirectory);
            $imageSize = isset($statImage) ? $statImage['size'] : 0;
            $absoluteFilePath = $mediaDirectory->getAbsolutePath($basePathDirectory);
            $imageType = $this->mime->getMimeType($absoluteFilePath);
            $mediaPath = $this->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            return [
                [
                    'name' => $image,
                    'url' => $mediaPath . $basePathUrl,
                    'size' => $imageSize,
                    'type' => $imageType
                ]
            ];
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param $image
     * @return string
     */
    public function getImageUrl($image)
    {
        if (!$image) {
            return null;
        }
        try {
            $basePathUrl = $this->imageUploader->getBasePath() . "/" . $image;
            $mediaPath = $this->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            return $mediaPath . $basePathUrl;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @return ImageUploader
     */
    public function getUploader()
    {
        return $this->imageUploader;
    }

    /**
     * @param Article $article
     */
    public function prepareData(Article $article)
    {
        $value = $article->getData($this->fieldName);
        if ($imageName = $this->getUploadedImageName($value)) {
            if (array_key_exists('tmp_name', $value[0])) {
                $imageName = $this->checkUniqueImageName($imageName);
            }
            $article->setData($this->prefix . $this->fieldName, $value);
            $article->setData($this->fieldName, $imageName);
        } elseif (!is_string($value)) {
            $article->setData($this->fieldName, null);
        }
    }

    /**
     * @param Article $article
     */
    public function storedData(Article $article)
    {
        $value = $article->getData($this->prefix . $this->fieldName);
        if ($this->isTmpFileAvailable($value) && $imageName = $this->getUploadedImageName($value)) {
            try {
                $this->imageUploader->moveFileFromTmp($imageName);
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }

    /**
     * Gets image name from $value array.
     *
     * Will return empty string in a case when $value is not an array.
     *
     * @param array $value Attribute value
     * @return string
     */
    private function getUploadedImageName($value)
    {
        if (is_array($value) && isset($value[0]['name'])) {
            return $value[0]['name'];
        }

        return '';
    }

    /**
     * Check that image name exists in directory and return new image name if it already exists.
     *
     * @param string $imageName
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function checkUniqueImageName(string $imageName): string
    {
        $mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $imageAbsolutePath = $mediaDirectory->getAbsolutePath(
            $this->imageUploader->getBasePath() . DIRECTORY_SEPARATOR . $imageName
        );

        $imageName = Uploader::getNewFilename($imageAbsolutePath);

        return $imageName;
    }

    /**
     * Check if temporary file is available for new image upload.
     *
     * @param array $value
     * @return bool
     */
    private function isTmpFileAvailable($value)
    {
        return is_array($value) && isset($value[0]['tmp_name']);
    }
}
