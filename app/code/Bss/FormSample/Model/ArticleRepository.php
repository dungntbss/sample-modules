<?php
declare(strict_types=1);
namespace Bss\FormSample\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

class ArticleRepository
{
    /**
     * @var ArticleFactory
     */
    protected $articleFactory;

    /**
     * @var ResourceModel\Article
     */
    protected $resourceArticle;

    /**
     * @var ResourceModel\Article\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ArticleSearchResultsFactory
     */
    protected $searchResultsFactory;

    /**
     * @var Article\PreviewImage
     */
    protected $previewImg;

    /**
     * ArticleRepository constructor.
     * @param ArticleFactory $articleFactory
     * @param ResourceModel\Article $resourceArticle
     * @param ResourceModel\Article\CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ArticleSearchResultsFactory $searchResultsFactory
     * @param Article\PreviewImage $previewImg
     */
    public function __construct(
        ArticleFactory $articleFactory,
        ResourceModel\Article $resourceArticle,
        ResourceModel\Article\CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        ArticleSearchResultsFactory $searchResultsFactory,
        Article\PreviewImage $previewImg
    ) {
        $this->articleFactory = $articleFactory;
        $this->resourceArticle = $resourceArticle;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->previewImg = $previewImg;
    }

    public function getUploader()
    {
        return $this->previewImg->getUploader();
    }

    /**
     * @param Article $article
     * @throws CouldNotSaveException
     */
    public function save(Article $article)
    {
        try {
            $this->previewImg->prepareData($article);
            $this->resourceArticle->save($article);
            $this->previewImg->storedData($article);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __(
                    'Could not save article: %1',
                    $e->getMessage()
                ),
                $e
            );
        }
    }

    /**
     * @param $id
     * @return Article
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function get($id)
    {
        $article = $this->articleFactory->create();
        $this->resourceArticle->load($article, $id);
        if (!$article->getArticleId()) {
            throw NoSuchEntityException::singleField($this->resourceArticle->getIdFieldName(), $id);
        }
        return $article;
    }

    /**
     * @param Article|int
     * @throws StateException
     */
    public function delete($article)
    {
        try {
            if (!($article instanceof Article)) {
                $article = $this->get($article);
            }
            $this->resourceArticle->delete($article);
        } catch (\Exception $e) {
            throw new StateException(__('Cannot delete article'), $e);
        }
    }

    /**
     * @return Article
     */
    public function getNew()
    {
        return $this->articleFactory->create();
    }

    /**
     * @return ResourceModel\Article\Collection
     */
    public function getNewCollection()
    {
        return $this->collectionFactory->create();
    }

    public function duplicate(Article $article)
    {
        try {
            $article->unsetData(Article::ARTICLE_ID);
            $this->resourceArticle->save($article);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __(
                    'Could not save article: %1',
                    $e->getMessage()
                ),
                $e
            );
        }
    }

    /**
     * @param Article
     * @return Article
     */
    private function castProductIdBeforeSave($article)
    {
        if ($article->getProductId() === '') {
            $article->setProductId(null);
        }
        return $article;
    }
}
