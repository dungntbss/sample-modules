<?php
declare(strict_types=1);
namespace Bss\FormSample\Ui\DataProvider\Article\Form\Modifier;

use Bss\FormSample\Model\Article;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;

class PreviewImage implements ModifierInterface
{
    /**
     * @var Article\PreviewImage
     */
    protected $articleImg;

    /**
     * Image constructor.
     * @param Article\PreviewImage $articleImg
     */
    public function __construct(
        Article\PreviewImage $articleImg
    ) {
        $this->articleImg = $articleImg;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        if (isset($data['id'])) {
            $articleId = $data['id'];
            $previewImg = $data[$articleId][Article::PREVIEW_IMG];
            if ($previewImg) {
                $data[$articleId][$this->articleImg->getFieldName()] = $this->articleImg->getImageData($previewImg);
            }
        }
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }
}
