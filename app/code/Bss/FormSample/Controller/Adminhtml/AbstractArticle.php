<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml;

use Bss\FormSample\Model\ArticleRepository;
use Magento\Backend\App\Action;

abstract class AbstractArticle extends Action
{
    const ACTIVE_MENU = 'Bss_FormSample::article_index';

    /**
     * @var ArticleRepository
     */
    protected $articleRepository;

    /**
     * AbstractArticle constructor.
     * @param Action\Context $context
     * @param ArticleRepository $articleRepository
     */
    public function __construct(
        Action\Context $context,
        ArticleRepository $articleRepository
    ) {
        $this->articleRepository = $articleRepository;
        parent::__construct($context);
    }

    /**
     * @param string $title
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function getResultPage($title)
    {
        $resultPage = $this->resultFactory->create($this->resultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu(self::ACTIVE_MENU)
            ->addBreadcrumb(
                __('Bss - Article'),
                __('Bss - Article')
            )->addBreadcrumb(
                $title,
                $title
            );
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }
}
