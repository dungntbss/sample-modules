<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml\Index;

use Bss\FormSample\Controller\Adminhtml\AbstractArticle;
use Bss\FormSample\Model\ArticleRepository;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;

abstract class AbstractMassAction extends AbstractArticle implements HttpPostActionInterface
{
    /**
     * @var Filter
     */
    protected $collectionFilter;

    /**
     * AbstractMassAction constructor.
     * @param Action\Context $context
     * @param ArticleRepository $articleRepository
     * @param Filter $collectionFilter
     */
    public function __construct(
        Action\Context $context,
        ArticleRepository $articleRepository,
        Filter $collectionFilter
    ) {
        $this->collectionFilter = $collectionFilter;
        parent::__construct($context, $articleRepository);
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        try {
            $this->massAction();
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $this->_redirect('*/*');
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb
     */
    protected function getCollection()
    {
        return $this->collectionFilter->getCollection($this->articleRepository->getNewCollection());
    }

    /**
     * @return void
     * @throws LocalizedException
     */
    abstract protected function massAction();
}
